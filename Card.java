public class Card {
	private String suit;
	private String value;

    public Card(String value, String suit) {
        this.suit = suit;
        this.value = value;
    }

    public String getSuit() {
        return this.suit;
    }

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return this.value + " of " + this.suit;
    }
	
	public double calculateScore() {
		String[] suits = {"Clubs", "Diamonds", "Spades", "Hearts"};
        String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		int indexV = 0;
		int indexS = 0;
		double scoreV = 0.0;
		double scoreS = 0.0;
		
		for(int ival = 0; ival < values.length; ival++) {
			if (this.value.equals(values[ival])) {
				indexV = ival + 1;
			}
		}
		scoreV = indexV + 0.0;
		for(int isui = 0; isui < suits.length; isui++) {
			if (this.suit.equals(suits[isui])) {
				indexS = isui + 1;
			}
		}
		scoreS = indexS + 0.0;
		scoreS = scoreS/10.0;
		return scoreV + scoreS;
	}
}
