import java.util.Scanner;

public class SimpleWar {
	public static void main(String[] args) {
		
		Deck deck = new Deck();
		deck.shuffle();
		
		int player1 = 0;
		int player2 = 0;
		
		while(deck.length() > 0) {
		
		Card card1 = deck.drawTopCard();
		Card card2 = deck.drawTopCard();
		
		System.out.println(card1);
		System.out.println(card2);
		
		double card1Score = card1.calculateScore();
		double card2Score = card2.calculateScore();
		
		System.out.println(card1Score);
		System.out.println(card2Score);
		
		if(card1Score > card2Score) {
			System.out.println("Player 1 wins");
			player1++;
		} else {
			System.out.println("Player 2 wins");
			player2++;
		}
		
		System.out.println("Points for player 1 is: " + player1);
		System.out.println("Points for player 2 is: " + player2);
		}
		
		if(player1 > player2) {
			System.out.println("congrats to player 1!");
		} else {
			System.out.println("congrats to player 2!");
		}
	}
}